﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MenuControler : MonoBehaviour {
	public Toggle vibrationState;

	public void returnToMainScene(){
		Application.LoadLevel ("Main_Scene");
	}

	public void updateSBGBlack(){
		PlayerPrefs.SetString ("cameraColor", "black");
	}

	public void updateSBGWhite(){
		PlayerPrefs.SetString ("cameraColor", "white");
	}

	public void updateVibration(){
		if (vibrationState.isOn)
			PlayerPrefs.SetString ("vibration", "on");
		else
			PlayerPrefs.SetString ("vibration", "off");
	}
}
