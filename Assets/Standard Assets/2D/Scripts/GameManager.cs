﻿using UnityEngine;
using UnityEngine.Advertisements;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
//using Random = UnityEngine.Random;

/*
 * By Joseph Marino Tripp
 * For Red Wolf Soft
 * 
 * 
 */

/*
 * Notas del desarrollador
 * >Ignoren mis errores gramaticales
 * Lo indices son como siguen:
 * > 0 izquierda
 * > 1 arriba
 * > 2 derecha
 * > 3 abajo
 * 
 */


public class GameManager : MonoBehaviour {

	//Arreglos de librerias de donde sacar cosas
	public GameObject[] botonesColores;
	public GameObject botonesRojos;
	public GameObject[] botonesPower;
	public GameObject botonesGris;

	public Button menu;
	public Text instrucciones;
	public Text scoreText;
	public Text highscoreText;
	public Text finalScore;
	public GameObject barraTiempo;


	public Camera cam;
	//libreria de donde saca las instrucciones
	private	string[] colores;
	private Color auxColor;

	//en este arreglo guardo mis cosas para poder tener sus lugares en la memoria
	//4 Sensores 2 botones power 2 scores 1 Texto color 1 barra tiempo
	private GameObject[] elementosGui = new GameObject[10];
	//necesitan un padre!
	private Transform elementHolder;

	private int correctAnswer; //que inciso deberia de escoger para sacarla bien TODO: ver si sirve o si no que hago



	//que stado tendra el juego
	private int game_state;
	private int rand;
	//en que turno va el jugador
	private float turno;
	//tickers
	private float ticks;
	private float clock;
	private float minimumAnimTime;

	private bool showAds;

	private bool controlsEnabled;
	//cosas relativas al input
	private Touch initialTouch;
	private float swipeDistance;
	private bool hasSwiped;
	private Vector2 touchPos;
	private bool aTouch;

	// Use this for initialization
	void Start () {
		//contadores 
		int cA;

		//auxiliares para posicionar unidades en la pantalla
		float auxHorizontal=0f, auxVertical=-0.5f;
			//alto/ancho
		float auxResolucion=16/9;
			//10 unidades de unity es el ancho
		auxResolucion = auxResolucion/10;//TODO: fix
			//rotaciones
		Quaternion auxRotacion = Quaternion.Euler (0f,0f,0f);

		//colores posibles organizados como lo estarian usualmente
		colores=new string[4];
		colores [0] = "Yellow";		//Izquierda Originalmente
		colores [1] = "Blue";		//Arriba O.
		colores [2] = "Red";		//Derecha O.
		colores [3] = "Green";		//Abajo O.

		//comienza a instanciar
		//instancia todos los botones de colores(4)
		for(cA=0;cA < botonesColores.Length;cA++){
			GameObject toInstantiate = botonesColores[cA];
			GameObject instanceBotones = Instantiate(toInstantiate, 
			                                  new Vector3(auxHorizontal,auxVertical,0f), 
			                                  auxRotacion) 
				as GameObject;
			//cambia el padre a algo local
			instanceBotones.transform.SetParent(elementHolder);
			//los guarda para poder hacer referencia en la memoria
			elementosGui[cA] = instanceBotones;//hay 4 lugares llenos
		}

		//instancia el boton de power y su hijo el transparente
		for (cA=0; cA < botonesPower.Length; cA++) {
			GameObject instancePower = Instantiate(botonesPower[cA],
			                                      new Vector3(auxHorizontal,auxVertical,0f),
			                                      auxRotacion)
				as GameObject;
			//cambia el padre a algo local
			instancePower.transform.SetParent (elementHolder);
			//los guarda para poder hacer referencia en la memoria
			elementosGui[cA+4] = instancePower;//hay 6 lugares llenos
		}

		GameObject instanceBotonesRojos = Instantiate (botonesRojos, 
		                                               new Vector3(auxHorizontal,auxVertical,0f),
		                                               auxRotacion)
			as GameObject;

		instanceBotonesRojos.transform.SetParent (elementHolder);
		elementosGui [6] = instanceBotonesRojos;

		GameObject instanceBarra = Instantiate (barraTiempo,new Vector3(0f,5f,0f),auxRotacion) as GameObject;
		instanceBarra.transform.SetParent (elementHolder);
		elementosGui [7] = instanceBarra;//hay 7 lugares llenos

		/*
		GameObject instanceGrises = Instantiate (botonesGris, new Vector3(auxHorizontal,auxVertical,0f),auxRotacion) as GameObject;
		instanceGrises.transform.SetParent (elementHolder);
		elementosGui [8] = instanceGrises;
		*/
		if (PlayerPrefs.GetString ("cameraColor").Equals ("black")) {
			cam.backgroundColor = Color.black;
			Debug.Log("paso por black");
		} else {
			cam.backgroundColor = Color.white;
			Debug.Log("paso por white");
		}
			//


		game_state = 0;			//comienza el juego
		turno = 1f;				//no llevas ningun turno
		ticks = 0f;				//se inicia el reloj
		clock = 1f;				//se inicializa clock
		minimumAnimTime = 1f;	//por lo menos un segundo para las animaciones de giro

		controlsEnabled = true;
		showAds = true;
		//INPUT shit
		initialTouch = new Touch ();
		hasSwiped = false;
		touchPos = new  Vector2();
		aTouch = false;

		//ver si sirve como input TODO: terminar
		if (Application.platform != RuntimePlatform.IPhonePlayer)
		{
			// use the input stuff
			aTouch = Input.GetMouseButton(0);
			touchPos = Input.mousePosition;
		} else {
			// use the iPhone Stuff
			aTouch = (Input.touchCount > 0);
			touchPos = Input.touches[0].position;
		}

		finalScore.text = "";
		scoreText.text = "SCORE: 0";
		highscoreText.text = "HI: " + PlayerPrefs.GetFloat("Highscore").ToString();

		instrucciones.text = "";
		instrucciones.color = Color.red;

	}
	
	// Update is called once per frame
	void Update () {
		//tener un contador de ticks siempre es bueno
		ticks += Time.deltaTime;

		if (Application.platform != RuntimePlatform.IPhonePlayer)
		{
			// use the input stuff
			aTouch = Input.GetMouseButton(0);
			touchPos = Input.mousePosition;
		} else {
			// use the iPhone Stuff
			aTouch = (Input.touchCount > 0);
			touchPos = Input.touches[0].position;
		}


		if(controlsEnabled){
			foreach (Touch t in Input.touches) {
				switch (t.phase) {
				case TouchPhase.Began:
					initialTouch = t;
					break;
				case TouchPhase.Moved:
					if (!hasSwiped) {
						float dtaX = initialTouch.position.x - t.position.x;
						float dtaY = initialTouch.position.y - t.position.y;
						swipeDistance = Mathf.Sqrt ((dtaX * dtaX) + (dtaY * dtaY));
						bool swipedReverse = Mathf.Abs (dtaX) > Mathf.Abs (dtaY);
						if (swipeDistance > 50f) {
							if(swipedReverse){
								if(dtaX >0){
									//scoreText.text="1";
									jugar (0);
									//izquierda
								}else{
									//scoreText.text="2";
									jugar (2);
									//derecha
								}
							}else{
								if(dtaY > 0){
									jugar(3);
									//scoreText.text="3";
									//abajo
								}else{
									jugar (1);
									//scoreText.text="4";
									//arriba
								}
							}
							hasSwiped = true;
						}
					}
					break;
				case TouchPhase.Ended:
					initialTouch = new Touch ();
					hasSwiped = false;
					break;
				}
			}
		}

	
		
		PlayerPrefs.SetFloat ("Highscore",turno - 1 > PlayerPrefs.GetFloat("Highscore")?
		                      turno-1:
		                    PlayerPrefs.GetFloat("Highscore"));
		highscoreText.text =" HI: " + PlayerPrefs.GetFloat ("Highscore").ToString();

		switch(game_state){
		case -4:
			if (showAds){
				if(Advertisement.isReady()){
					Advertisement.Show(null, new ShowOptions {
						pause = true,
						resultCallback = result => {
							Debug.Log(result.ToString());
						}
					});
					showAds = false;
				}
			}
			if (aTouch && (!Advertisement.isShowing)){
				game_state = 0;
				ticks = 0;
				showAds = true;
			}else if(aTouch){/*
				game_state = 0;
				ticks = 0;
				showAds = true;*/
			}

			break;
		case -3:
			if(ticks>1){
				for(int cA = 0; cA < 4 ; cA++){
					elementosGui[cA].SendMessage("cambiarSpritesOriginales");
				}
				//finalScore.text ="" + (turno-2);
				game_state = -4;		//comienza el juego
				turno = 1f;				//no llevas ningun turno
				ticks = 0f;				//se inicia el reloj
				clock = 1f;	
				instrucciones.text = "";
				showAds = true;
			}


			break;
		case -2:
			finalScore.text ="" + (turno-1);
			if(ticks > 1 && aTouch){
				for(int cA = 0; cA<4 ; cA++){
					elementosGui[cA].SendMessage("Desaparecer");
					game_state = -3;
				}
			}


			break;
		case -1://hubo un error
			controlsEnabled = false;
		 
			for(int cA = 0; cA<4 ; cA++){
				elementosGui[cA].SendMessage("cambiarSpritesARojos");
				//elementosGui[cA].SendMessage("Desaparecer");
			}
			instrucciones.color = Color.red;
			elementosGui[7].SendMessage("CambiarColor",colores[2]);
			instrucciones.text = "Error";
			game_state = -2;
			ticks = 0;

				//Application.LoadLevel("Main_Scene");

			break;

		default:


			//instrucciones.SendMessage ("Desaparecer");
			//botones 
			elementosGui[4].SendMessage("Aparecer");
			elementosGui[5].SendMessage("Intermitir");
			finalScore.text = "";

			game_state = 1;
			ticks = 0;
				
			//menu.interactable = true;
			break;
		case 1:
			//shreked este es un caso empty
			//no es es defaulta para que el default reinicie el juego
			if(aTouch && ticks > 1){
				if(!Advertisement.isShowing){
					game_state = 2;
					controlsEnabled = true;
				}
			}
			break;
		case 2:
			//desaparecer los botones
			elementosGui[4].SendMessage("Desaparecer");
			elementosGui[5].SendMessage("Desaparecer");
			//menu.interactable = false;
			menu.BroadcastMessage("Desaparecer");
			game_state = 3;
			ticks = 0;
			break;
		case 3:
			//despues de un segundo
			if(ticks > 1){
				//aparece los sensores
				for(int cA = 0 ; cA < 4 ; cA++)
					elementosGui[cA].SendMessage("Aparecer");
				ticks = 0;
				//cambia a la sig fase
				game_state = 4;
			}
			controlsEnabled = true;
			break;
		case 4:
			clock = (3-Mathf.Log(turno,4f)) > 0.300f?
				(3-Mathf.Log(turno,4f)):
					0.300f;
			ticks = 0;
			scoreText.text="SCORE: "+(turno-1);
			game_state = 5;
			break;
		case 5:

			//tiene que haber por lo menos un giro
			//primer turno

			//en el primer turno solo se elige un color al azar
			if(turno == 1){
				rand = Random.Range(0,3);

				//instrucciones.text = colores[rand].ToUpper();
				correctAnswer = rand;

				elementosGui[7].SendMessage("CambiarColor",colores[rand]);
				instrucciones.text = colores[rand].ToUpper(); 
				
				instrucciones.color = colores[rand] == "Yellow"?Color.yellow:
					colores[rand] == "Red"?Color.red:
						colores[rand] == "Blue"?Color.blue:Color.green;

			//turno 2 al 10

			//en el turno 2 asta el 10 se gira y se escoge un color al azar
			}else if(turno <= 10){

				int rand = Random.Range(0,3);
				//rota el arreglo que traduce al ingles
				rotarNVeces(colores,rand);
				//rotan los elementos
				for(int cA=0;cA<4;cA++)
					elementosGui[cA].SendMessage("giroPorTicksEnTicks",rand);

				rand = Random.Range(0,4);

				elementosGui[7].SendMessage("CambiarColor",colores[rand]);
				instrucciones.text = colores[rand].ToUpper(); 
				
				instrucciones.color = colores[rand] == "Yellow"?Color.yellow:
					colores[rand] == "Red"?Color.red:
						colores[rand] == "Blue"?Color.blue:Color.green;


				correctAnswer = rand;


			}else{
				//a partir del turno 11

				int rand = Random.Range(0,3);
				//rota el arreglo que traduce al ingles
				rotarNVeces(colores,rand);
				//rotan los elementos
				for(int cA=0;cA<4;cA++)
					elementosGui[cA].SendMessage("giroPorTicksEnTicks",rand);
				
				rand = Random.Range(0,4);
				
				//instrucciones.text=colores[rand].ToUpper();
				correctAnswer = rand;
				instrucciones.text = colores[rand].ToUpper(); 
				
				rand= Random.Range(0,4);

				elementosGui[7].SendMessage("CambiarColor",colores[rand]);

				
				instrucciones.color = colores[rand] == "Yellow"?Color.yellow:
					colores[rand] == "Red"?Color.red:
						colores[rand] == "Blue"?Color.blue:Color.green;


			}
			turno += 1;
			game_state = 6;

			auxColor = instrucciones.color;
			auxColor.a = 0;
			instrucciones.color = auxColor;
			
			break;
		case 6:
			if(ticks>minimumAnimTime){

				elementosGui[7].SendMessage("Achicar",clock);


				auxColor = instrucciones.color;
				auxColor.a = 1;
				instrucciones.color = auxColor;

				game_state = 7;
			}
			break;
		case 7:
			if(ticks > clock+minimumAnimTime){ //si te tardaste de mas
				game_state = -1; //te equivocaste
				ticks = 0;
			}
			break;
		}

	}

	private void jugar(int queJugo){
		elementosGui [7].SendMessage ("achicarInmediato");

		instrucciones.text = "";


		for(int cA = 0;cA<4;cA++){
			elementosGui[cA].SendMessage("brillar", colores[correctAnswer] );
		}



		if (queJugo == correctAnswer) {

			game_state = 4;
			ticks = 0;
		} else {
			game_state = -1;
			ticks = 0;
		}
	}

	private void initJuego(){
		game_state = 2;
	}

	void Awake() {
		if (Advertisement.isSupported) {
			Advertisement.allowPrecache = true;
			Advertisement.Initialize ("37364");
		} else {
			Debug.Log("Platform not supported");
		}
	}

	/*
	void OnGUI() {
		if(GUI.Button(new Rect(10, 10, 150, 50), Advertisement.isReady() ? "Show Ad" : "Waiting...")) {
			// Show with default zone, pause engine and print result to debug log
			Advertisement.Show(null, new ShowOptions {
				pause = true,
				resultCallback = result => {
					Debug.Log(result.ToString());
				}
			});
		}
	}
	/*
	/*
	private void jugarTurno(int giros){
		for(int cA=0;cA<4;cA++)
			elementosGui[cA].SendMessage("giroPorTics",giros);
		//rotarNVeces (colores,giros);

	}

	private void onGUI(){

	}*/

	//despues de una rotacion mando a este arreglo todo para que gire x tics en sentido de las manecillas del relojveces
	private void rotarNVeces<T>(T[] things,int n){
		n = n % things.Length;

		//un auxiliar del mismo tamano
		T[] aux = new T[things.Length];

		for (int cA = 0; cA < things.Length; cA++) 
			aux[(cA+n) % things.Length] = things[cA];

		for(int cB = 0; cB < things.Length; cB++)
			things[cB] = aux[cB];
	}

	public void CambiarATest(){
		Application.LoadLevel ("Menu_Opciones");
	}
}/*
		foreach (Touch t in Input.touches) {
			switch (t.phase) {
			case TouchPhase.Began:
				initialTouch = t;
				break;
			case TouchPhase.Moved:
				if (!hasSwiped) {
					float dtaX = initialTouch.position.x - t.position.x;
					float dtaY = initialTouch.position.y - t.position.y;
					swipeDistance = Mathf.Sqrt ((dtaX * dtaX) + (dtaY * dtaY));
					bool swipedReverse = Mathf.Abs (dtaX) > Mathf.Abs (dtaY);
					if (swipeDistance > 50f) {
						if (swipedReverse && dtaX > 0) {
							//scorehg.text = "izquierda";
							//jugCirculo (1);
						} else if (swipedReverse && dtaX <= 0) {
							//scorehg.text = "dera";
							//jugCirculo (3);
						} else if (!swipedReverse && dtaY > 0) {
							//scorehg.text = "abajo";
							//jugCirculo (2);
						} else if (!swipedReverse && dtaY <= 0) {
							//scorehg.text = "upp";
							//jugCirculo (0);
						}
						hasSwiped = true;
					}
				}
				break;
			case TouchPhase.Ended:
				initialTouch = new Touch ();
				hasSwiped = false;
				break;
			}*/
