﻿using UnityEngine;
using System.Collections;

public class Gui_Behavior : MonoBehaviour {
	public Sprite[] spritesPosibles;
	public Sprite[] spritesPosiblesBlurr;
	public Sprite spritesRojos;
	public int cualEs;
	public float animationSpeed;
	public float rotationSpeed;

	private bool estaApareciendo;			//si esta apareciendo
	private bool estaDesapareciendo;		//si esta desapareciendo
	private bool estaIntermitente;			//self expanatory
	private bool estaGirando;
	private bool estaBrillando;
	private Color auxColor;					//nadamas para guardar el color posible

	private float ticks;					//un contador para segundos
	private float ticksBrillo;
	private int rotacionActual;				// cuanto ha rotado

	private int ticksGiroEnCola;			//cuantos giro hay pendientes en la animacion
	private Quaternion qInicial;
	private float zInicial,zFinal;


	private int auxRotacionInicial;

	// Use this for initialization
	void Start () {
		estaApareciendo = false;//no tiene animacion
		estaDesapareciendo = false;//no tiene animacion
		estaBrillando = false;
		ticks = 0;//el reloj esta en 0
		ticksBrillo = 0;

		rotacionActual = 0;
		//los sensores empiezan invisibles

		auxColor=this.GetComponent<SpriteRenderer>().color;
		auxColor.a = 0;//completamente translucido
		this.GetComponent<SpriteRenderer>().color=auxColor;

		transform.localScale = new Vector3 (0.75f,
		                                    0.75f,
		                                    0.75f);
	}
	
	// Update is called once per frame
	void Update () {
		ticks += Time.deltaTime * animationSpeed;

		rotacionActual = rotacionActual % 4;

		//bloque que maneja el aparecer/desaparecer continuo
		if (estaIntermitente) {
			//ticks+=Time.deltaTime*animationSpeed;
			if(ticks>=1){
				estaApareciendo=estaApareciendo?false:true;
				estaDesapareciendo=estaApareciendo?false:true;
				ticks=0;
			}
		}

		//este bloque de texto maneja el update de aparecer
		if (estaApareciendo && ticks > cualEs*0.200){//si esta apareciendo
			if (ticks < 1 + cualEs*0.200) {//desde 0 a 1
				//ticks+=Time.deltaTime * animationSpeed;//incrementa con cada frame
				auxColor=GetComponent<SpriteRenderer>().color;
				auxColor.a = ticks - cualEs*0.200f;//cambia lo translucido dependiendo de los tics
				GetComponent<SpriteRenderer>().color=auxColor;
			}else{
				estaApareciendo=false;//si ya se paso 
				auxColor=GetComponent<SpriteRenderer>().color;
				auxColor.a = 1;//completamente visible
				GetComponent<SpriteRenderer>().color=auxColor;
				ticks=0;
			}
		}

		//este bloque maneja el ipdate de desaparecer
		if (estaDesapareciendo) {
			if (ticks < 1) {
				//ticks += Time.deltaTime * animationSpeed;//incrementa con cada frame
				auxColor = GetComponent<SpriteRenderer> ().color;
				auxColor.a = 1 - ticks;//cambia lo translucido dependiendo de los tics
				GetComponent<SpriteRenderer> ().color = auxColor;
			} else {
				estaDesapareciendo = false;//si ya se paso 
				auxColor = GetComponent<SpriteRenderer> ().color;
				auxColor.a = 0;//completamente translucido
				GetComponent<SpriteRenderer> ().color = auxColor;
				ticks = 0;
			}
		}

		if (estaGirando) {
			if(ticks<1){
				transform.rotation = Quaternion.Euler(0,0,Mathf.Lerp(zInicial,zFinal,ticks));
			}else{
				transform.rotation = Quaternion.Euler(0,0,zFinal);
				estaGirando=false;
			}
		}

		if(estaBrillando){
			ticksBrillo += Time.deltaTime;
			if(ticksBrillo>0.2f){
				GetComponent<SpriteRenderer> ().sprite = spritesPosibles[cualEs-1];
				//GetComponents<SpriteRenderer> ().sprite = spritesPosiblesBlurr[cualEs-1];
				estaBrillando= false;
				ticksBrillo = 0;
			}
		}

	}

	void brillar(string s){
		s = s.ToLower();

		ticksBrillo = 0;
		bool brillar = false;

		switch (s) {
		case "yellow":
			if (cualEs == 1)
				brillar = true;
			break;
		case "blue":
			if (cualEs == 2)
				brillar = true;
			break;
		case "red":
			if (cualEs == 3)
				brillar = true;
			break;
		case "green":
			if (cualEs == 4)
				brillar = true;
			break;
		}

		if (brillar) {
			GetComponent<SpriteRenderer> ().sprite = spritesPosiblesBlurr[cualEs-1];
			estaBrillando = true;
		}
	}

	public int getTicksGiro(){
		rotacionActual = rotacionActual % 4;
		return rotacionActual;
	}

	void setTicksGiro(int aux){
		//TODO: hacer que gire instantaneamente
		rotacionActual = aux % 4;
	}

	void Aparecer(){//cambia las constantes de animacion
		estaApareciendo = true;
		estaDesapareciendo = false;
		estaIntermitente = false;
		ticks = 0;
	}

	void Desaparecer(){
		estaApareciendo = false;
		estaDesapareciendo = true;
		estaIntermitente = false;
		ticks = 0;
	}

	void DesaparecerInmediato(){
		auxColor = GetComponent<SpriteRenderer> ().color;
		auxColor.a = 0;//completamente translucido
		GetComponent<SpriteRenderer> ().color = auxColor;
		estaApareciendo = false;
		estaDesapareciendo = false;
		estaIntermitente = false;
		ticks = 0;
	}

	void Intermitir(){
		estaApareciendo = true;
		estaDesapareciendo = false;
		estaIntermitente = true;
		ticks = 0;
	}
	void giroPorTicks(int ticksDeGiro){
		ticksDeGiro = ticksDeGiro % 4;
		transform.Rotate (new Vector3(0f,0f,1f),90*ticksDeGiro*(-1));
		rotacionActual += ticksDeGiro;
		rotacionActual %= 4;
	}

	void giroPorTicksEnTicks(int ticksDeGiro){//vector 2 por que recibe dos parametros
		estaGirando = true;
		ticksDeGiro %= 4;
		auxRotacionInicial = rotacionActual;

		//animationSpeed = cuanto == 0 ?1:1/cuanto;

		rotacionActual += ticksDeGiro;
		rotacionActual %= 4;

		switch (auxRotacionInicial %= 4){
		default:
			zInicial = 0f;
			break;
		case 1:
			zInicial = 270f;
			break;
		case 2:
			zInicial = 180f;
			break;
		case 3:
			zInicial = 90f;
			break;
		}

		zFinal = (zInicial + (rotacionActual-auxRotacionInicial)*(-90)) /* % 360*/;

		ticks = 0;
	}

	void cambiarSpritesOriginales(){
		GetComponent<SpriteRenderer> ().sprite = spritesPosibles [cualEs - 1];
	}

	void cambiarSpritesARojos(){
		GetComponent<SpriteRenderer> ().sprite = spritesRojos;
	}
}
