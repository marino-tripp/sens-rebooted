﻿using UnityEngine;
using System.Collections;

public class Text_Behavior : MonoBehaviour {

	private bool estaApareciendo;
	private bool estaDesapareciendo;
	private Color auxColor;
	private float tics;

	// Use this for initialization
	void Start () {
		estaApareciendo = false;
		estaDesapareciendo = false;
		tics = 0;
		//auxColor = this.GetComponent<uint> ();
		//auxColor.a = 0;
		//this.GetComponent<Text> ().color= auxColor;
	}
	
	// Update is called once per frame
	void Update () {

	
	}

	void Aparecer(){//cambia las constantes de animacion
		estaApareciendo = true;
		estaDesapareciendo = false;
	//	estaIntermitente = false;
		tics = 0;
	}
	
	void Desaparecer(){
		estaApareciendo = false;
		estaDesapareciendo = true;
	//	estaIntermitente = false;
		tics = 0;
	}
}
