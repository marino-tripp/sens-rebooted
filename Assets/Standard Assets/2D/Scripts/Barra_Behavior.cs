﻿using UnityEngine;
using System.Collections;

public class Barra_Behavior : MonoBehaviour {

	public Sprite[] spritesPosibles;

	private bool estaCreciendo;
	private bool estaAchicando;
	private float animationSpeed;
	private float auxPantalla;
	private float ticks;
	private Vector3 auxScale;
	
	// Use this for initialization
	void Start () {
		estaAchicando = false;
		estaCreciendo = false;
		ticks = 0;
		auxPantalla = 0.52f;
		auxScale = transform.localScale;
		auxScale.y = 1.5f;
		transform.localScale = auxScale;
	
	}
	
	// Update is called once per frame
	void Update () {
		ticks += Time.deltaTime * animationSpeed * auxPantalla;

		if(estaCreciendo){
			if(ticks < auxPantalla){
				auxScale = transform.localScale;
				auxScale.x = ticks;
				transform.localScale = auxScale;
			}else{
				auxScale = transform.localScale;
				auxScale.x = auxPantalla;
				transform.localScale = auxScale;
				estaCreciendo = false;
				ticks = 0;
			}
		}

		if(estaAchicando){
			if(ticks < auxPantalla){
				auxScale = transform.localScale;
				auxScale.x = auxPantalla-ticks;
				transform.localScale = auxScale;
			}else{
				auxScale = transform.localScale;
				auxScale.x = 0;
				transform.localScale = auxScale;
				estaAchicando = false;
				ticks = 0;
			}
		}
	
	}

	void Achicar(float segundos){
		ticks = 0;
		animationSpeed = segundos==0?
			1:
				1 / segundos;
		estaAchicando = true;
		estaCreciendo = false;
	}

	void Crecer(float segundos){
		ticks = 0;
		animationSpeed = segundos==0?
			1:
				 1 / segundos;
		estaAchicando = false;
		estaCreciendo = true;
	}

	void CrecerInmediato(){
		ticks = 1;

		auxScale = transform.localScale;
		auxScale.x = auxPantalla;
		transform.localScale = auxScale;

		estaAchicando = false;
		estaCreciendo = false;

	}

	void achicarInmediato(){
		ticks = 1;
		
		auxScale = transform.localScale;
		auxScale.x = 0;
		transform.localScale = auxScale;
		
		estaAchicando = false;
		estaCreciendo = false;
	}

	void CambiarColor(string color){
		switch(color.ToLower()){
		default:
			GetComponent<SpriteRenderer> ().sprite=spritesPosibles[0];
			break;
		case "blue":
			GetComponent<SpriteRenderer> ().sprite=spritesPosibles[1];
			break;
		case "red":
			GetComponent<SpriteRenderer> ().sprite=spritesPosibles[2];
			break;
		case "green":
			GetComponent<SpriteRenderer> ().sprite=spritesPosibles[3];
			break;
		}
	}
}
